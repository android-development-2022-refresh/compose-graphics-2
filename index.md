---
title: Compose Graphics 2
template: main-full.html
---

## Introduction

We continue our Graph editor application, adding support for
* Blinking like-typed shapes when clicked
* Dragging shapes
* Drawing lines between objects (that stay connected when the shapes are moved)

> Note that I walk through several intentional errors during these videos and explain why they occur/how to fix them. The user tap/drag interaction can be trickier to get right than it seems, so I wanted to highlight common problems and show that sometimes an "obvious" design may not be correct...

## Videos

Total video time for this module: 1:57:28
            
### Jetpack Compose Graphics: Blinking and Internal Compiler Errors (Fall 2021) (52:55)

<iframe width="800" height="450" src="https://www.youtube.com/embed/4UZt1KOd8ow" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
             
### Jetpack Compose Graphics: Dragging and Connecting (Fall 2021) (1:04:33)

<iframe width="800" height="450" src="https://www.youtube.com/embed/SqscYM8G8KY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            
## Example Source

See: [https://gitlab.com/android-development-2022-refresh/compose-graphics-2](https://gitlab.com/android-development-2022-refresh/compose-graphics-2)
